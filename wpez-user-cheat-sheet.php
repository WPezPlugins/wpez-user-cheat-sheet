<?php
/**
 * Plugin Name: WPezPlugins: User Cheat Sheet
 * Plugin URI: https://gitlab.com/WPezPlugins/wpez-user-cheat-sheet
 * Description: A cheat sheet (i.e., admin dashboard widget) for the WP things users wish they could remember, but often do not.
 * Version: 0.0.1
 * Author: Mark F. Simchock (Chief Executive Alchemist @ Alchemy United)
 * Author URI: https://AlchemyUnited.com/utm_source=wpez_ucs
 * Text Domain: wpez_ucs
 * License: GPLv2 or later
 *
 * @package WPezForceThumbnail
 */

namespace WPezUserCheatSheet;

if ( ! defined( 'ABSPATH' ) || ! defined( 'WP_CONTENT_DIR' )) {
	exit;
}


/**
 * Plugin settings collected in a single place, and is filterable.
 *
 * @param string $key The key to the array for the setting you want.
 * @param string $fallback If the key is not found, return this.
 *
 * @return string
 */
function getSetting( string $key, $fallback = '' ) {

	$arr = array(
		'option_group'         => 'general',
		'option_group_title'   => __( 'WPezPlugins: User Cheat Sheet - Settings', 'wpez_ucs' ),
		'option_group_desc'    => __( 'For more info: <a href="https://gitlab.com/WPezPlugins/wpez-user-cheat-sheet">https:// gitlab.com/WPezPlugins/wpez-user-cheat-sheet</a>' ),
		'option_namespace'     => 'wpez_user_cheat_sheet_options',
		'title_widget_default' => __( 'WPezPlugins: User Cheat Sheet', 'wpez_ucs' ),
		'widget_error'         => __( 'User Cheat Sheet is not available. You may want to contact the site admin.', 'wpez_ucs' ),
		'title_deactivate'     => __( '&nbsp;', 'wpez_ucs' ),
		'label_deactivate'     => __( 'Delete plugin\'s options from the database on deactivation', 'wpez_ucs' ),
		'title_user_cap'       => __( 'User Capability', 'wpez_ucs' ),
		'title_widget_title'   => __( 'Widget Title', 'wpez_ucs' ),
		'title_path_to'        => __( 'Path to Cheat Sheet<br>(from .../wp-content/)', 'wpez_ucs' ),
		'path_to_file_default' => 'wpez-user-cheat-sheet.html',
	);

	$arr_new = apply_filters( __NAMESPACE__ . '\settings', array(), $arr );
	if ( is_array( $arr_new ) ) {
		$arr = array_merge( $arr, $arr_new );
	}

	if ( isset( $arr[ $key ] ) ) {
		return $arr[ $key ];
	}
	return $fallback;
}

register_activation_hook( __FILE__, __NAMESPACE__ . '\registerActivation' );
/**
 * Callback for register_activation_hook().
 *
 * @return void
 */
function registerActivation() {

	$str_option    = getSetting( 'option_namespace' );
	$str_file_html = getSetting( 'path_to_file_default' );
	$arr_option    = get_option( $str_option, array() );

	$str_path_slug = \str_replace( WP_CONTENT_DIR, '', get_stylesheet_directory() );

	// If there are no settings (stored as an option in the db), then set the defaults.
	if ( empty( $arr_option ) ) {

		$arr_option = array(
			'delete_on_deactivate' => false,
			'user_capability'      => 'switch_themes',
			'widget_title'         => getSetting( 'title_widget_default' ),
			'path_to_cheat_sheet'  => $str_path_slug . '/' . $str_file_html,
		);
		add_option( $str_option, $arr_option );
	}
}


register_deactivation_hook( __FILE__, __NAMESPACE__ . '\registerDeactivation' );
/**
 * Callback for register_deactivation_hook(). Deletes the plugin's options if necessary.
 *
 * @return void
 */
function registerDeactivation() {

	$str_option_name = getSetting( 'option_namespace' );
	$bool_check      = (bool) getOption( $str_option_name, 'delete_on_deactivate' );

	if ( true === $bool_check ) {
		delete_option( $str_option_name );
	}
}




add_action( 'wp_dashboard_setup', __NAMESPACE__ . '\userCheatSheetDashboardWidget' );

/**
 * Callback for wp_dashboard_setup. Adds the dashboard widget, if the user has the capability.
 *
 * @return void
 */
function userCheatSheetDashboardWidget() {

	$str_option_name = getSetting( 'option_namespace' );
	$str_user_cap    = trim( (string) getOption( $str_option_name, 'user_capability' ) );

	if ( current_user_can( $str_user_cap ) ) {

		$str_widget_title = trim( (string) getOption( $str_option_name, 'widget_title' ) );
		wp_add_dashboard_widget(
			'wpez_user_cheat_sheet',
			esc_html__( $str_widget_title ),  // phpcs:ignore WordPress.WP.I18n.NonSingularStringLiteralText
			__NAMESPACE__ . '\userCheatSheetDashboardWidgetView'
		);
		// Note: The $global $wp_meta_boxes global could be "filtered" to hack around the phpcs:ignore above.
	}
}

/**
 * Callback for wp_add_dashboard_widget(). Renders the dashboard widget content from the path/file in the settings.
 *
 * @return void
 */
function userCheatSheetDashboardWidgetView() {

	// https:// developer.wordpress.org/reference/classes/wp_filesystem_base/ .
	WP_Filesystem();
	global $wp_filesystem;

	$str_option_name = getSetting( 'option_namespace' );
	$str_path        = ltrim( trim( (string) getOption( $str_option_name, 'path_to_cheat_sheet' ) ), '/' );

	// get the content of a file with the path .
	$str_file_contents = $wp_filesystem->get_contents( WP_CONTENT_DIR . '/' . esc_attr( $str_path ) );

	if ( $str_file_contents ) {
		// Note: this will strip out the <html>, <head>, <body>, etc. tags. Which is what we want.
		echo wp_kses_post( $str_file_contents );
	} else {
		echo '<p>' . esc_attr( getSetting( 'widget_error' ) ) . '</p>';
	}
}

add_action( 'admin_init', __NAMESPACE__ . '\adminInit' );

/**
 * Adds the settings page using the WP Settings API.
 *
 * @return void
 */
function adminInit() {

	$str_option_name  = getSetting( 'option_namespace' );
	$str_option_group = getSetting( 'option_group' );

	register_setting(
		$str_option_group,
		$str_option_name,
		array( 'sanitize_callback' => __NAMESPACE__ . '\sanitizeCallback' ),
	);

	$str_field = 'delete_on_deactivate';
	add_settings_field(
		$str_option_name . '_' . $str_field,
		(string) getSetting( 'title_deactivate' ),
		__NAMESPACE__ . '\cbInputCheckbox',
		$str_option_group,
		$str_option_name . '_section_id',
		array(
			'option' => $str_option_name,
			'field'  => $str_field,
			'label'  => (string) getSetting( 'label_deactivate' ),
		),
	);

	$str_field = 'user_capability';
	add_settings_field(
		$str_option_name . '_' . $str_field,
		(string) getSetting( 'title_user_cap' ),
		__NAMESPACE__ . '\cbInputText',
		$str_option_group,
		$str_option_name . '_section_id',
		array(
			'option'    => $str_option_name,
			'field'     => $str_field,
			'label_for' => $str_option_name . '_' . $str_field,
			'class'     => 'regular-text',
		),
	);

	$str_field = 'widget_title';
	add_settings_field(
		$str_option_name . '_' . $str_field,
		(string) getSetting( 'title_widget_title' ),
		__NAMESPACE__ . '\cbInputText',
		$str_option_group,
		$str_option_name . '_section_id',
		array(
			'option'    => $str_option_name,
			'field'     => $str_field,
			'label_for' => $str_option_name . '_' . $str_field,
			'class'     => 'regular-text',
		),
	);

	$str_field = 'path_to_cheat_sheet';
	add_settings_field(
		$str_option_name . '_' . $str_field,
		(string) getSetting( 'title_path_to' ),
		__NAMESPACE__ . '\cbInputText',
		$str_option_group,
		$str_option_name . '_section_id',
		array(
			'option'    => $str_option_name,
			'field'     => $str_field,
			'label_for' => $str_option_name . '_' . $str_field,
			'class'     => 'regular-text',
		),
	);

	// Create settings section.
	add_settings_section(
		$str_option_name . '_section_id',
		(string) getSetting( 'option_group_title' ),
		__NAMESPACE__ . '\sectionDescription',
		$str_option_group,
	);
}

/**
 * Callback for add_settings_field(). Renders the description for the settings section.
 *
 * @return void
 */
function sectionDescription() {

	$str_desc = getSetting( 'option_group_desc' );
	echo wp_kses_post( $str_desc );
}

/**
 * Callback for the register_setting() above. Sanitizes the input.
 *
 * @param array<string, string> $arr_args The array of values from the Settings API based inputs.
 *
 * @return array<string, string|bool>
 */
function sanitizeCallback( array $arr_args ) {

	foreach ( $arr_args as $key => $value ) {

		switch ( $key ) {
			case 'delete_on_deactivate':
				$arr_args[ $key ] = sanitizeInputCheckboxSingle( $value );
				break;
			case 'user_capability':
				$arr_args[ $key ] = sanitizeInputText( (string) $value );
				break;
			case 'path_to_cheat_sheet':
				// TODO - change to something that at least checks for file.ext .
				$arr_args[ $key ] = sanitizeInputText( (string) $value );
				break;
		}
	}
	return $arr_args;
}


/**
 * Outputs a checkbox.
 *
 * @param array<string, string> $args Args for the checkbox markup.
 *
 * @return void
 */
function cbInputCheckbox( array $args = array() ) {

	if ( isset( $args['option'], $args['field'], $args['label'] ) ) {

		$str_option = $args['option'];
		$str_field  = $args['field'];
		$str_label  = $args['label'];

		$str_post_type = '';
		if ( isset( $args['post_type'] ) ) {
			$str_post_type = $args['post_type'];
			$str_label     = str_replace( '%post_type%', $str_post_type, $str_label );
		}

		$value = getOption( $str_option, $str_field, '' );
		$value = sanitizeInputCheckboxSingle( $value );

		?>
		<label for="<?php echo esc_attr( $str_option . '_' . $str_field ); ?>">
			<input id="<?php echo esc_attr( $str_option . '_' . $str_field ); ?>" type="checkbox" name="<?php echo esc_attr( $str_option . '[' . $str_field . ']' ); ?>" <?php echo checked( $value ); ?>> <?php echo esc_attr( $str_label ); ?>
		</label>
		<?php
	}
}

/**
 * Sanitizes a single value from a checkbox input.
 *
 * @param mixed $value Value to sanitize (i.e., return a bool of).
 *
 * @return bool
 */
function sanitizeInputCheckboxSingle( $value = '' ) {

	return ( ! empty( $value ) ) ? true : false;
}

/**
 * Outputs a text input.
 *
 * @param array <string, string> $args  Args for the text input markup.
 *
 * @return void
 */
function cbInputText( array $args ) {

	if ( isset( $args['label_for'], $args['option'], $args['field'] ) && ! empty( $args['label_for'] ) ) {

		$str_option    = (string) $args['option'];
		$str_field     = (string) $args['field'];
		$str_label_for = (string) $args['label_for'];

		$str_class = 'wpez-input-text';
		if ( isset( $args['class'] ) && ! empty( esc_attr( $args['class'] ) ) ) {
			$str_class .= ' ' . (string) esc_attr( $args['class'] );
		}

		$value = (string) getOption( $str_option, $str_field, '' );

		$str_fn_sanitize = '';
		// Note: function_exists() and call_user_func() both expect the __NAMESPACE__ to be included in the string arg.
		if ( isset( $args['fn_sanitize'] ) && ! empty( esc_attr( $args['fn_sanitize'] ) ) && function_exists( esc_attr( $args['fn_sanitize'] ) ) ) {
			$str_fn_sanitize = (string) $args['fn_sanitize'];
			$value           = (string) call_user_func( $str_fn_sanitize, $value ); // fyi - PHPStan doesn't like this.
		} else {
			$value = (string) sanitizeInputText( $value );
		}

		?>
		<input id="<?php echo esc_attr( $str_label_for ); ?>" 
		class="<?php echo esc_attr( $str_class ); ?>" type="text" 
		name="<?php echo esc_attr( $str_option . '[' . $str_field . ']' ); ?>" 
		value="<?php echo esc_attr( $value ); ?>">
		<?php
	}
}

/**
 * Sanitizes a single value from a text input.
 *
 * @param string $value Value to sanitize (i.e., return a string of).
 *
 * @return string
 */
function sanitizeInputText( string $value ) {
	return sanitize_text_field( $value );
}

/**
 * Wrapper / helper for WP's standard get_option().
 *
 * @param string      $str_option_name Option name.
 * @param string      $str_field       Option field.
 * @param string|bool $mix_default     Default value to return if option is not set.
 *
 * @return string|bool
 */
function getOption( $str_option_name = '', $str_field = '', $mix_default = '' ) {

	$arr = get_option( $str_option_name, array() );

	if ( is_array( $arr ) && isset( $arr[ $str_field ] ) ) {
		return $arr[ $str_field ];
	}
	return $mix_default;
}



