# WPezPlugins: User Cheat Sheet

### WordPress plugin that adds WP admin dashboard widget for helpful "notes" for your users. 

**The dashboard widget's content comes from an .html file that your specify, so it can be unique for each project/client.**

For example, maybe you have a handful of CSS helper classes for adding to blocks, or a couple of other "don't forget"s. This plugin gives you a place for things like that, a place that can't be forgotten, misplaced, deleted, etc. by your client(s) / user(s), or yourself. 

There are four settings:
- Checkbox - Delete plugin's options from the database on deactivation. Default: Unchecked
- Text input - User capability needed to add the dashboard widget to their dashboard. Default: switch_themes
- Test input - Title of the dashboard widget: Default: WPezPlugins: User Cheat Sheet
- Text input - path to the .html file you want to use for the dashboard widget's content. Default: /themes/{current active theme}/wpez-user-cheat-sheet.html

The defaults will be added when the plugin is activated and no previous option data is detected.

The plugin's settings can get found under Admin > Settings > General.

Note: The contents of the file wpez-user-cheat-sheet.html is escaped / sanitized with the wp_kses_post() function prior to being echo'ed into the dashboard widget. This allows you maintain a standard html file for the cheat sheet, knowing that the unnecessary tags (e.g., \<head>, \<body>, etc.) will be stripped out. It also means you can also use any markup tags - links, bold, lists, etc. - that the wp_kses_post() function allows.

It's recommended / suggested that you keep the wpez-user-cheat-sheet.html file in the projects theme / child theme as it's likely the cheat sheet will be related to the project / theme. That said, ultimately, you can have it anywhere within the /wp-content/ folder. 

Finally, be mindful of what you're putting in the wpez-user-cheat-sheet.html and the fact that you might be committing that file to a repo. Even if it's a private repo, accidents happen. Credentials, passwords, and such belong in a password manager.  
